from enums import Visibility


def filter_dataset(dataset, user, context, pagination):
    filtered_dataset = list(
        filter(lambda item: filter_item(None, item, user, context), dataset))
    return paginate(filtered_dataset, pagination)


def filter_item(parent, item, user, context, key=None):
    # object-level check
    check_result = check_item(item, user, context)

    # item is restricted, remove from results
    if check_result == Visibility.NONE:
        if key is not None:  # sub object removal
            del parent[key]
        else:
            return False

    # keep item in a "blurred" state so user can request access from app
    if check_result == Visibility.BLUR:
        blur_item(item)

    # keep item by default and recursively process sub objects
    else:
        for key, value in list(item.items()):
            if isinstance(value, dict) and key != 'meta':
                filter_item(item, value, user, context, key)

    return True


def check_item(item, user, context):
    if 'meta' in item:
        return check_restriction(item['meta'], user, context)

    return Visibility.OK


def check_restriction(meta_props, user, context):
    # ===== To complete =====
    # Process visibility between
    # - object (source, legal_fw, type, validity, etc)
    # - user (role, fonction, mission, level, tags) 
    # - context rights (attentat, pandemie, lieu de connexion...)

    # check for a default rule if no explicit one has been found for current user & context
    if '*' in meta_props and isinstance(meta_props['*'], Visibility):
        return meta_props['*']

    return Visibility.OK


def blur_item(item):
    id = item['id'] if 'id' in item else None
    item.clear()
    item['id'] = id
    item['label'] = '**********'
    item['blurred'] = True


def paginate(dataset, pagination):
    min = pagination[0]
    max = pagination[1]
    return dataset[min:max]
