from filter_logic import filter_dataset
from data.users import users
from data.objects import dataset
from data.contexts import contexts
import json

# manually select contextual input here
current_user = users[0] if users else {}
current_context = contexts[0] if contexts else {}

pagination = [0, 10]


def print_context_info():
    print("______________________")
    print("User: " + str(current_user))
    print("Context: " + str(current_context))


def print_dataset(dataset):
    print("______________________")
    print("Dataset:")
    print(json.dumps(dataset, indent=2, default=str))


def run():
    print_context_info()
    resultset = filter_dataset(
        dataset, current_user, current_context, pagination)
    print_dataset(resultset)


if __name__ == "__main__":
    run()
