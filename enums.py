from enum import IntEnum


class UserRole(IntEnum):
    USER = 0
    AGENT = 1
    MANAGER = 2
    ADMIN = 3
    SUPER_ADMIN = 4


class Visibility(IntEnum):
    NONE = -1
    BLUR = 0
    OK = 1