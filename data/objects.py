from enums import Visibility, UserRole

dataset = [{
    "id": 1,
    "label": "Object one",
    "tags": "123",
    "meta": {
        "*": Visibility.BLUR,
        "roles": {
            UserRole.ADMIN: Visibility.OK
        }
    }
},
    {
    "id": 2,
    "label": "Object two",
    "test": "432543",
    "visibleProp": {
        "label": "test visible prop",
        "test": "123"
    },
    "restrictedProp": {
        "label": "test hidden prop",
        "subProp": {
            "label": "test sub prop",
            "meta": {
            }
        }
    }
}]
